#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <pthread.h>

#define NTHREADS 10


//creating a global array to keep track of the commands that are being provided by the user
char *commands[900];
char *directories[900];
int num_of_commands = 0;
int num_elements_command = 0;

//the commandstruct structure is meant to contain the number of commands and the array of commands
typedef struct __mycommands{
	int num_commands;
	char *holding_commands[900];
	} __mycommands;

//concat() function is meant to concatenate 2 strings(i.e path and filename) into 1 string.
char *concat(char *s1,char *s2){
    char *result = malloc(strlen(s1) + strlen(s2) + 1); 
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}


//path() provides different paths that a file can be located in and executed.
void path(){
	for(int i = 1; i < num_of_commands; i++){
		char *path = concat(commands[i],"/");
		directories[i-1] = path;
		}
	directories[num_of_commands-1] = NULL;
	}
	
	
	
//cd() changes the currennt directory of the wish bash shell
void cd(char *d){
	int return_value;
	return_value = chdir(d);
	if(return_value == 0){
		printf("The directory has been changed to %s\n",d);
		}else{
			char error_message[30] = "An error has occured\n";
			write(STDERR_FILENO,error_message,strlen(error_message));
			exit(1);
			}
	}
	
	
//strremove() takes out all of the newline characters from the inputs given by the user
char *strremove(char *str, const char *sub) {
    size_t len = strlen(sub);
    if (len > 0) {
        char *p = str;
        while ((p = strstr(p, sub)) != NULL) {
            memmove(p, p + len, strlen(p + len) + 1);
        }
    }
    return str;
}



//execute_file() is used to execute the commands provided by the user
int execute_file(){
	char *myargs[num_of_commands+1];
	if(num_of_commands > 1){
		for(int i = 0; i < num_of_commands; i++){
				myargs[i] = strdup(commands[i]);
			}
		myargs[num_of_commands] = NULL;
		int child_process = fork();
		if(child_process < 0){
			char error_message[30] = "An error has occured\n";
			write(STDERR_FILENO,error_message,strlen(error_message));
			exit(1);
			}
		if(child_process == 0){
			execvp(myargs[0],myargs);
			}else{
				wait(NULL);
				}
		}
	if(num_of_commands == 1){
		myargs[0] = commands[0];
		myargs[1] = NULL;
		int child_process = fork();
		if(child_process < 0){
			char error_message[30] = "An error has occured\n";
			write(STDERR_FILENO,error_message,strlen(error_message));
			exit(1);
			}
		if(child_process == 0){
			execvp(myargs[0],myargs);
			}else{
				wait(NULL);
				}
		}
	return 0;
	}

//the execute_command() function is responsble for executing the command and the parameters passed into the function.
void *execute_command(void *arg){
	__mycommands *h_comm = (__mycommands *) arg;
	int nums = h_comm->num_commands;
	char* myargs[nums+1];
	if(nums > 1){
		for(int i =0; i < nums; i++){
			myargs[i] = h_comm->holding_commands[i];
		}
		myargs[nums] = NULL;
		int child_process = fork();
		if(child_process < 0){
			char error_message[30] = "An error has occured\n";
			write(STDERR_FILENO,error_message,strlen(error_message));
			exit(1);
			}
		if(child_process == 0){
			execvp(myargs[0],myargs);
			}else{
				wait(NULL);
				}
		}
	if(nums == 1){
		myargs[0] = h_comm->holding_commands[0];
		myargs[1] = NULL;
		int child_process = fork();
		if(child_process < 0){
			char error_message[30] = "An error has occured\n";
			write(STDERR_FILENO,error_message,strlen(error_message));
			exit(1);
			}
		if(child_process == 0){
			execvp(myargs[0],myargs);
			}else{
				wait(NULL);
				}
		}
		//h_comm->num_commands = 0; 
	}

//check_file() function checks to see if a file can be found in any of the directories and if it is executable
int check_file(char *c){
	char *path;
	int access_value;
	int index = 0;
	while(directories[index] != NULL){
		path = concat(directories[index],c);
		access_value = access(path,X_OK);
		if(access_value == 0){
			execute_file();
			return 1;
			}
		index += 1;
		}
	return 0;
	}

//parallel_check_file() is responsible for checking whether a file provided through parallel commands is executable.
int parallel_check_file(char *c){
	char *path;
	int access_value;
	int index = 0;
	while(directories[index] != NULL){
		path = concat(directories[index],c);
		access_value = access(path,X_OK);
		if(access_value == 0){
			return 1;
			}
		index += 1;
		}
	return 0;
	}



//stringInput() function splits the input provided by the user into tokens and places them in an array
int split_input(char *s){
	const char *newline = "\n";
	int index = 0;
	char *token = strtok(s," ");
	while (token != NULL) {	
	   token = strremove(token,newline);   
	   commands[index] = token;
	   index += 1; 
	   num_of_commands += 1; 
       token = strtok(NULL, " "); 
    } 
    commands[index] = NULL;
    return 0; 
	}

//the check_redirect() function checks to see whether the commands that the user provides contains '>'
int check_redirect(){
	for(int i = 0; i < num_of_commands; i++){
		if(strstr(commands[i],">")){
			return 1;
			}
		}
		return 0;
	}

//the redirect() function performs the redirects the output of the commands provided into the given file
void redirection(){
		int rc = fork();
		if (rc < 0){
			printf("An error has occured");
			exit(1);
		}else if(rc == 0){
			char *output_file = commands[num_of_commands-1];
			char *new_output = concat("./",output_file);
			commands[num_of_commands - 1] = NULL;
			commands[num_of_commands - 2] = NULL;
			num_of_commands = num_of_commands - 2;
			char *myargs[num_of_commands+1];
			int index = 0;
			while(commands[index] != NULL){
				myargs[index] = strdup(commands[index]);
				index += 1;
				}
			myargs[index] = NULL;
			close(STDOUT_FILENO);
			open(new_output,O_CREAT|O_RDWR|O_TRUNC, S_IRWXU);
			execvp(myargs[0],myargs);
		}else{
			wait(NULL);
			}
	}

//the check_parallel() function is responsible to checkking if the command provided by the user involves the execution of parallel commands
int check_parallel(){
	for(int i = 0; i < num_of_commands; i++){
		if(strstr(commands[i], "&")){
			return 1;
			}
		}
		return 0;
	}
	


	
//the parallel_commands() function is responsible for performing the parallel commands provided by the user
int parallel_commands(){
	__mycommands comm;
	comm.num_commands = 0;
	//char *holding_current_command[num_of_commands];
	pthread_t threads[NTHREADS];
	int holding_index = 0;
	int thread_order = 0;
	//check each of the commands provided by the user to find out if an executable command may be obtained
	for(int i = 0; i < num_of_commands; i++){
		int result = parallel_check_file(commands[i]);
		if(result == 1){ 
			int j = i;
			while(!(strstr(commands[j],"&"))){
				comm.holding_commands[holding_index] = commands[j];
				holding_index += 1;
				comm.num_commands += 1;
				j += 1;
				if(j == num_of_commands){
					break;
					}
			}
			holding_index = 0;
			//pthread_create(&threads[thread_order],NULL,execute_command,(void*)holding_current_command); //creating thread
			pthread_create(&threads[thread_order],NULL,execute_command,&comm);
			pthread_join(threads[thread_order],NULL);
			//increment the tread_order by one so that another thread may be executed
			thread_order += 1;
			for(int i = 0; i < comm.num_commands; i++){
				comm.holding_commands[i] = NULL;
				}
			comm.num_commands = 0;
		}
	}
	return 1;
}
	
//the main function of the wish command is going to simulate a shell program.
int main(int argc,char *argv[]){
	//When no filename is provided, the interactive mode of the wish shell is activated.
	if(argc == 1){
		directories[0] = "/bin/";
		directories[1] = NULL;
		char *input;
		size_t input_size = 100;
		printf("wish>");
		input = (char *)malloc(input_size * sizeof(char));
		getline(&input, &input_size, stdin);
		while(!(strstr(input,"exit"))){
			split_input(input);
			if(strstr(commands[0],"cd")){
				if(num_of_commands == 1 || num_of_commands > 2){
					printf("cd should take one argument!\n");
					}else{
						cd(commands[1]);
					}
				}
			if(strstr(commands[0],"path")){
				if(num_of_commands == 1){
					directories[0] = NULL;
					printf("Please provide path(s) otherwise you would ONLY be able to execute built in commands\n");
					printf("Example: path /bin /usr/bin\n");
					}
					
				if(num_of_commands > 1){
					path();
					}
				}
			if(num_of_commands > 1){
				//check to ensure that the commands proposed contains the redirect symbol.
				if(check_redirect() == 1){
					//call the function that handles redirection
					redirection();
					}
				//check to ensure that parallel commands are to be executed	
				if(check_parallel() == 1){
					parallel_commands();
					}
				}
			if(num_of_commands == 1){
				check_file(commands[0]);
				}
			num_of_commands = 0;
			printf("wish>");
			getline(&input, &input_size, stdin);
			}
		}
	//When a filename is provided, the batch mode of the wish shell is activated.
	if(argc == 2){
		FILE *file = fopen(argv[1],"r");
		if(file != NULL){
			directories[0] = "/bin/";
			directories[1] = NULL;
			char *line;
			char input[512];
			while(fgets(input,512,file)){
				line = input;
				if(!(strstr(line,"exit"))){
					split_input(line);
					if(strstr(commands[0],"cd")){
						if(num_of_commands == 1 || num_of_commands > 2){
							//do nothing
						}else{
							cd(commands[1]);
						}
					}
					if(strstr(commands[0],"path")){
						if(num_of_commands == 1){
							directories[0] = NULL;
							}
						if(num_of_commands > 1){
							path();
							}
						}
					if(num_of_commands == 1){
						check_file(commands[0]);
						}
					num_of_commands = 0;
					}
				}
				fclose(file);
			}
		}		
	
	return(0);
}
