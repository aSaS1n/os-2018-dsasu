#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]){
	//if the user just calls the my-grep.c function without providing any parameters,
	//print out: "my-grep: searchterm[file ...]"
	if (argc == 1){
		printf("my-grep: searchterm[file...]");
		exit(1);
	}
	if(argc ==  2){
	//if the user just inputs the my-grep.c function and the search term, ask the user to input a text where it can search from
		printf("You seem to have entered a search term without entering a text file to search for it from!\n");
		printf("Please enter some text to search for your search term from:\n");
		int size = 100;
		int count = 0;
		char *newString;
		newString =  malloc(size);
		char character = getchar();
		while(!(character == '\n')){
			if(count != size){
				//place the character in the newString array
				newString[count] = character;
				//printf("%c", character);
				count += 1;
			}else{
				newString =  realloc(newString,2*size);
				newString[count] = character;
				count += 1;
			}
			character = getchar();
		}
		//convert the array into a string
		count += 1;
		newString[count] = '\0';
		if (strstr(newString,argv[1])){
			printf("%s\n",newString);
		}else{
			printf("The search text that you provided could not be found in the sentence provided!\n");
		}
		exit(0);
	}
	//if the number of arguments that the user provides are more than one,
	//first  check to see if the user provided legitimate files
	if(argc > 2){
		for(int i = 2; i < argc; i++){
			FILE *file = fopen(argv[i], "r");
			if( file != NULL){
				//allocate memory to store the characters in the file
				int size = 100;
				int count = 0;
				char *newline;
				newline = malloc(size);
				char letter = fgetc(file);
				//read each character from the file until the end of the file is reached
				while(letter != EOF){
					if(letter != '\n'){
						if(count != size){
							newline[count] = letter;
							count += 1;
							}else{
								newline = realloc(newline, 2 * size);
								newline[count] = letter;
								count += 1;
								}
						letter = fgetc(file);
						}else{
							//if the character which is read is a newline character, it means
							//that an entire sentence has been read so convert the array into a string and check
							//to see if the word you are searching for is inside the sentence before moving on to the next sentence
							count += 1;
							newline[count] = '\0';
							if(strstr(newline,argv[1])){
								printf("%s\n",newline);
								}
							newline = malloc(size);
							count = 0;
							letter = fgetc(file);
							}
					}
					fclose(file);
		}
	}

}
	return(0);
}
