#include <stdio.h>

int main(int argc, char *argv[]){
	//First check to ensure that the user has provided any filenames as parameters to the program
	if (argc == 1){
		printf("You have not entered any files!");
		return (0);
	 }
 	if(argc > 1){
		for(int i = 1; i < argc; i++){
			//To read the content in the file, first open the file
			FILE *file = fopen(argv[i],"r");
			//check to ensure that the file can be read without error. 
			//in the event in which the file cannot be read, return from the program.
			if(file != NULL){
				char input[512];
				while(fgets(input, 512, file)){
					printf("%s",input);
			 	}
			 }else{
				int filePos = i + 1;
				printf("File:%d, could not be opened!",filePos);
				return(1);
			}
			fclose(file);
 		}
	}
	return (0);
}
